import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';

import { Calendar } from '@ionic-native/calendar';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
    request : any;
    flag : boolean;
    tanggal : string;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                private viewCtrl : ViewController,
                private toastCtrl : ToastController,
                private calendar : Calendar) {
        this.request = this.navParams.get('request');
        this.flag = this.navParams.get('flag');
        let time = new Date(this.request['date']);
        let date = time.toDateString();
        this.tanggal = date;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ModalPage');
        
    }

    acceptrequest(){
        this.request['status'] = "Accepted";
        let toast = this.toastCtrl.create({
            message: 'Meeting Request Has Been Accepted',
            duration: 3000,
            position: 'top'
        });
        toast.present();
        this.calendar.createEvent(this.request['desc'],this.request['location'],'',new Date(this.request['date']), new Date(this.request['end'])).then(data => {
            console.log("MASUK KE CALENDAR");
            console.log(data);
        });
        this.back();
    }

    rejectrequest(){
        this.request['status'] = "Rejected";
        let toast = this.toastCtrl.create({
            message: 'Meeting Request Has Been Rejected',
            duration: 3000,
            position: 'top'
        });
        toast.present();
        this.back();
    }

    back(){
        this.viewCtrl.dismiss(this.request);
    }

}
