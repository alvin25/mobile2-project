import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { CalendarEvent } from 'angular-calendar';
import { CalendarService } from '../../services/calendarservice';

import { DayPage } from '../day/day';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { take } from 'rxjs/operator/take';

/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {

    date: any;
    email : string;
    today : Date;
    colors : any;
    event : CalendarEvent;
    allevent : CalendarEvent[] = [];
    tanggal : Date;

    meeting : Observable<any>;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                    private alertCtrl : AlertController,
                    private calendarSvc : CalendarService,
                    private loadingCtrl : LoadingController,
                    private storage : Storage) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CalendarPage');
    }
    ngOnInit(){
        let loading = this.loadingCtrl.create({
            content: 'Fetching data...'
          });
        
        loading.present();

        this.today = new Date(Date.now());

        this.email = this.navParams.data;

        this.storage.set('to',this.email);

        this.calendarSvc.getAllMeeting().subscribe(data => {
            console.log(data);
            this.getalldata(data).then(data => {
                console.log('data event dari firebase');
                console.log(this.allevent);
                console.log('length dari all event' + this.allevent.length);
                loading.dismiss();
                this.allevent = this.allevent;
                return;
            })
            return;
        });
    }

    getalldata(data) : Promise<any> {
        data.forEach(element => {
            if(element['email'] == this.email){
                this.colors = element['color'];
                let date = new Date();
                date = element['date'];
                this.event = {
                    start: new Date(date),
                    end : new Date(element['end']),
                    title: element['desc'],
                    color: this.colors.red
                }
                this.allevent.push(this.event);
                this.event = null;
            }
        })
        return new Promise(resolve =>
        {
            resolve(this.allevent);
        });
    }

    openEvent(event){
        console.log(event.event);

        let start = new Date();
        start = event.event.start;
        let start_hour = start.getHours().toString();
        start_hour = this.addZero(start_hour);
        let start_minutes = start.getMinutes().toString();
        start_minutes = this.addZero(start_minutes);
        let time_start = start_hour + ':' + start_minutes;

        let end = new Date();
        end = event.event.end;
        let end_hour = end.getHours().toString();
        let end_minutes = end.getMinutes().toString();
        let time_end = end_hour + ':' + end_minutes;

        let alert = this.alertCtrl.create({
            title: event.event.title,
            subTitle: 'jam ' + time_start + 'sampai ' + time_end,
            buttons: ['Dismiss']
        });
        alert.present();
    }

    addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    dayclicked(event){
        this.navCtrl.push(DayPage, event.day);
    }

}
