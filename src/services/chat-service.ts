import { AuthService } from './authservice';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/toPromise';
import { AngularFireDatabase  } from 'angularfire2/database';
import { Storage  } from '@ionic/storage';
import { ChatData } from '../interface/chatData.interface';
import { ChatListData } from '../interface/chatListData.interface';
import { ChatMessage } from '../interface/chatMessage.interface';


export class UserInfo {
    id: string;
    name?: string;
    avatar?: string;
}

@Injectable()
export class ChatService {

    constructor(public http: Http,
                public events: Events,
                private afd: AngularFireDatabase,
                private storage:Storage,
                public authSrvc: AuthService) {
    }

    sendMsg(iUid, chatData){
        let uid = this.authSrvc.getActiveUser().uid;
        this.afd.list(`/userDetails/${uid}/chat/${iUid}`).push(chatData);
        let key=this.afd.list(`/userDetails/${iUid}/chat/${uid}`).push(chatData).key;
        console.log("key push="+key);
    }

    sendMsgAlt(chatID, chatData, userData, intercolutorData){
        this.afd.list('/chatList/'+chatID).push(chatData);
        let tgl=new Date(Date.now());
        console.log("date now="+tgl);
        let strDate=tgl.toString();
        console.log("date str="+strDate);
        let userDat:ChatData={
            chatID:chatID,
            interlucotorID:userData['uid'],
            interlucotorName:userData['name'],
            interlucotorEmail:userData['email'],
            lastMessage:chatData['message'],
            date:strDate
        }
        let interlocutorDat:ChatData={
            chatID:chatID,
            interlucotorID:intercolutorData['uid'],
            interlucotorName:intercolutorData['name'],
            interlucotorEmail:intercolutorData['email'],
            lastMessage:chatData['message'],
            date:strDate
        }
        this.afd.object('/userDetails/'+userData['uid']+'/chat/'+chatID).update(interlocutorDat);
        this.afd.object('/userDetails/'+intercolutorData['uid']+'/chat/'+chatID).update(userDat);
    }

    getMsg(iUid){
        let uid = this.authSrvc.getActiveUser().uid;
        return this.afd.list(`/userDetails/${uid}/chat/${iUid}`).valueChanges();
    }

    getMsgAlt(chatID){
        return this.afd.list('/chatList/'+chatID).valueChanges();
    }

    getChatList(uid){
        return this.afd.list('/userDetails/'+uid+'/chat').valueChanges();
    }

    firstTimeChat(uid, iUid, userData, interlocutorData){
        let chatList:ChatListData={
            user1:uid,
            user2:iUid

        }
        let tgl=new Date().getDate();
        let strDate=tgl.toString();
        let pushKey=this.afd.list('/chatList').push(chatList).key;
        let userDat:ChatData={
            chatID:pushKey,
            interlucotorID:iUid,
            interlucotorName:userData['name'],
            interlucotorEmail:userData['email'],
            lastMessage:'',
            date:tgl
        }
        let interlocutorDat:ChatData={
            chatID:pushKey,
            interlucotorID:uid,
            interlucotorName:interlocutorData['name'],
            interlucotorEmail:interlocutorData['email'],
            lastMessage:'',
            date:tgl
        }
        this.afd.object('/userDetails/'+uid+'/chat/'+pushKey).set(interlocutorDat);
        this.afd.object('/userDetails/'+iUid+'/chat/'+pushKey).set(userDat);
        return pushKey;
    }

    

    // getChatId(uid){
    //     return this.afd.list('/userDetails/'+uid+'/chat').valueChanges();
    // }

    // mockNewMsg(msg) {
    //     const mockMsg: ChatMessage = {
    //         messageId: Date.now().toString(),
    //         userId: '210000198410281948',
    //         userName: 'Jeff',
    //         userAvatar: './assets/to-user.jpg',
    //         toUserId: '140000198202211138',
    //         time: Date.now(),
    //         message: msg.message,
    //         status: 'success'
    //     };

    //     setTimeout(() => {
    //         this.events.publish('chat:received', mockMsg, Date.now())
    //     }, Math.random() * 1800)
    // }

    // getMsgList(): Promise<ChatMessage[]> {
    //     const msgListUrl = './assets/mock/msg-list.json';
    //     return this.http.get(msgListUrl)
    //     .toPromise()
    //     .then(response => response.json().array as ChatMessage[])
    //     .catch(err => Promise.reject(err || 'err'));
    // }

    // sendMsg(msg: ChatMessage) {
    //     return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
    //     .then(() => this.mockNewMsg(msg));
    // }

    // getUserInfo(): Promise<UserInfo> {
    //     const userInfo: UserInfo = {
    //         id: '140000198202211138',
    //         name: 'asd',
    //         avatar: './assets/user.jpg'
    //     };
    //     return new Promise(resolve => resolve(userInfo));
    // }

}
