import { alamat } from './../../interface/alamat.interface';
import { userData } from '../../interface/userdata.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../services/authservice';
import { TabsPage } from '../tabs/tabs';
import { Storage  } from '@ionic/storage';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {
    currentuser:userData[];

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public toastCtrl: ToastController,
        public authService: AuthService,
        private loadingCtrl : LoadingController,
        private storage: Storage ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

    signUp(form){
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        
        loading.present();

        // this.authService.register(form.value['email'],form.value['password'])
        // .then(data =>{
        //     console.log("berhasil register");
        //     let user = [];
        //     user['name'] = form.value['name'];
        //     user['gender'] = form.value['gender'];
        //     user['dob'] = form.value['dob'];
        //     user['job'] = form.value['job'];
        //     this.currentuser = user;
        //     this.authService.getActiveUser().getIdToken()
        //     .then((token: string) => {
        //         this.authService.registrationData(token, this.currentuser)
        //             .subscribe(() => {
        //                 console.log("berhasil register");
        //                 loading.dismiss();
        //                 this.navCtrl.push(TabsPage);
        //             },
        //             error=>{console.log(error)})
        //     });
        // })
        this.authService.register(form.value['email'],form.value['password'])
        .then(data =>{
            console.log("berhasil register");
            let uid=this.authService.getActiveUser().uid;
            this.storage.set('uid',uid);
            this.storage.set('email', data.email);
            let user = [];
            user['name'] = form.value['name'];
            user['gender'] = form.value['gender'];
            user['dob'] = form.value['dob'];
            user['job'] = form.value['job'];
            user['email'] = form.value['email'];
            //this.currentuser = user;
            // let amt=[];
            // amt['kodepos']="333";
            // amt['rt']='11';
            // user['almt']=amt;
            this.authService.registrationDataFireAngular(user);
            console.log("berhasil register");
            loading.dismiss();
            this.navCtrl.push(TabsPage);
            // this.authService.getActiveUser().getIdToken()
            // .then((token: string) => {
            //     this.authService.registrationData(token, this.currentuser)
            //         .subscribe(() => {
            //             console.log("berhasil register");
            //             loading.dismiss();
            //             this.navCtrl.push(TabsPage);
            //         },
            //         error=>{console.log(error)})
            // });
        })
        .catch(err => {
        loading.dismiss();
        const toast = this.toastCtrl.create({
            message: err.message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
        })
    }

}
