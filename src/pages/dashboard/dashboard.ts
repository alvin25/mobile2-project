import { Chat } from './../chat/chat';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, 
    ActionSheetController, LoadingController, ToastController, ModalController} from 'ionic-angular';
import { RequestformPage } from '../requestform/requestform';
import { CalendarPage } from '../calendar/calendar';
import { ModalPage } from '../modal/modal';
import { AuthService } from '../../services/authservice';
import { CalendarEvent } from 'angular-calendar';
import { CalendarService } from '../../services/calendarservice';
import { Promise } from 'q';
import { resolve } from 'path';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
    cat: string = "received"; // default button

    colors : any;
    event : CalendarEvent;
    allevent : any;

    email : string;

    requestinbox : any = [];
    myrequest : any = [];

    temp: any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public actionSheetCtrl: ActionSheetController,
        private loadingCtrl : LoadingController,
        private authService : AuthService,
        private calendarSvc : CalendarService,
        private toastCtrl : ToastController,
        private storage : Storage,
        private modalCtrl : ModalController
        ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DashboardPage');
        this.ngOnInit();
    }

    ngOnInit(){
        let loading = this.loadingCtrl.create({
            content: 'Fetching data...'
          });
        
        loading.present();

          this.storage.get('email').then(data => {
              console.log('dashboard' + this.email);
            this.email = data;
            this.calendarSvc.getAllMeeting().subscribe(data => {
                console.log(data);
                this.allevent = data;
                this.getalldata(data).then(data => {
                    this.requestinbox = this.requestinbox;
                    this.myrequest = this.myrequest;
                    console.log('request yang masuk ke gw nih');
                    console.log(this.requestinbox);
                    console.log('request yg gw kirimkan');
                    console.log(this.myrequest);
                    loading.dismiss();
                })
            });
        });
    }

    getalldata(data) : Promise<any> {
      this.requestinbox=[];
      this.myrequest=[];
        data.forEach(element => {
            if(element['email'] == this.email){
                this.requestinbox.push(element);
            }
            if(element['requester'] == this.email){
                this.myrequest.push(element);
            }
        })
        return Promise(resolve =>
        {
            resolve(true);
        });
    }


    openAlert(){
        let alert = this.alertCtrl.create({
            title: 'Search User by Email',
            inputs: [
              {
                name: 'email',
                placeholder: 'Email'
              }
            ],
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: data => {
                }
              },
              {
                text: 'Send',
                handler: data => {
                    if(data.email != null){
                        console.log(data.email);
                        if(data.email == this.email){
                          let toast = this.toastCtrl.create({
                            message: 'Cannot Insert Own Email',
                            duration: 3000,
                            position: 'top'
                          });
                        
                          toast.present();
                        }
                        else{
                          this.navCtrl.push(CalendarPage, data.email);
                        }
                    }
                    else{
                        let toast = this.toastCtrl.create({
                            message: 'Please Input Email',
                            duration: 3000,
                            position: 'top'
                          });
                        
                          toast.present();
                          this.openAlert();
                    }
                }
              }
            ]
          });
          alert.present();
    }

    
    presentActionSheet(request){
      const actionSheet = this.actionSheetCtrl.create({
        title: 'Meeting Options',
        buttons:[
          {
            text: 'Detail',
            handler: () => {
                this.temp = request;
                let detailModal = this.modalCtrl.create(ModalPage, {'request' : request, 'flag' : true});
                detailModal.onDidDismiss(data => {
                    
                    this.calendarSvc.updateMeeting(data);
                    
                  });
                detailModal.present();
            }
          },
          {
            text: 'Chat',
            handler: () => {
              //this.getQuoteDetails(quote);
              console.log('requester email='+request['requester']);
              this.authService.getUsers().valueChanges().take(1).subscribe(
                data=>{
                  data.forEach(
                    element=>{
                      console.log('user loop='+element['email']);
                      if(element['email']==request['requester']){
                        let user=[];
                        user['name'] = element['name'];
                        user['gender'] = element['gender'];
                        user['dob'] = element['dob'];
                        user['job'] = element['job'];
                        user['email'] = element['email'];
                        user['uid']=element['uid'];
                        this.navCtrl.push(Chat, user);
                        return;
                      }
                    }
                  )
                }
              )
            }
          },
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () =>{
              console.log('kensel');
            }
          }
        ]
      });
  
      actionSheet.present();
    }

    openModal(request){
        let detailModal2 = this.modalCtrl.create(ModalPage, {'request' : request, 'flag' : false});
        detailModal2.onDidDismiss(data => {
            console.log(data);
          });
        detailModal2.present();
    }

    doRefresh(refresher){
        this.ngOnInit();
        refresher.complete();
    }
}
