import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';

import  firebase  from 'firebase';
import { AuthService } from '../services/authservice';
import { AngularFirestore, AngularFirestoreModule, AngularFirestoreDocument } from 'angularfire2/firestore';

import { Firebase } from '@ionic-native/firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage:any = LoginPage;

  isSignedIn=false;

  @ViewChild('sideMenuContent') nav: NavController;

  constructor(platform: Platform,
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private menuCtrl : MenuController,
    public authSrvc: AuthService,
    private pushnotif: Firebase) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (!firebase.apps.length) {
        firebase.initializeApp({
          apiKey: "AIzaSyAbLoj7wLvSfwGf7PTUKnGWkjoJ3dLx748",
          authDomain: "temu-a6ded.firebaseapp.com"
        });
      }
      this.initializefcm();
      firebase.auth().onAuthStateChanged(user => {
        if(user) {
          //do something here if the user is logged in
          this.nav.setRoot(TabsPage);
        }
        else {
          console.log("logout firebase auth");
          this.menuCtrl.close();
          this.nav.setRoot(LoginPage);
          //do something here if the user is not logged in
        }
      });
      statusBar.styleDefault();
      setTimeout(() => {
        splashScreen.hide();
      }, 100);
    });
    
  }
    logout(){
        this.authSrvc.logout();
    }

    initializefcm()
    {
        this.pushnotif.getToken()
        .then(token => console.log(`The token is ${token}`))
        .catch(error => console.error('Error getting token', error));
        
        this.pushnotif.onTokenRefresh()
        .subscribe((token: string) =>
        {
            //register token untuk ke external api untuk menerima push notif
            console.log(token);
        });

        this.pushnotif.grantPermission();
        this.pushnotif.subscribe('chat');
        this.pushnotif.subscribe('request');
        this.pushnotif.hasPermission().then(data =>
        {
            this.pushnotif.onNotificationOpen()
            .subscribe((data => 
            { 
                if(data.wasTapped) console.log("Received in background"); 
                else
                {
                    console.log("Received in foreground");
                    if(data.tap == false)
                    {
                    }
                };
                }
            ));
        })
    }
}

