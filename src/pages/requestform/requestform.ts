import { userData } from '../../interface/userdata.interface';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../services/authservice';
import { TabsPage } from '../tabs/tabs';
import { CalendarPage } from '../calendar/calendar';
import { DashboardPage } from '../dashboard/dashboard';
import { ElementRef } from '@angular/core/src/linker/element_ref';
import { CalendarService } from '../../services/calendarservice';
import { CalendarEvent } from 'angular-calendar';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the RequestformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-requestform',
  templateUrl: 'requestform.html',
})
export class RequestformPage {
    // @ViewChild('desc') desc: ElementRef;

    email:string;
    to:string;
    location:string;
    time: any;
    desc:any;
    duration : number;

    date : Date;

    colors : any;
    event : CalendarEvent;

    number : number;
    
    constructor(public navCtrl: NavController, 
            public navParams: NavParams, 
            public loadingCtrl : LoadingController, 
            public authService: AuthService,
            private calendarSvc : CalendarService,
            private toastCtrl : ToastController,
            private storage : Storage) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RequestformPage');
    }

    ngOnInit(){

        this.storage.get('email').then(data => {
            console.log(data);
            this.email = data;
        })

        this.storage.get('to').then(data => {
            this.to = data;
        });

        this.duration = 1;

        let event = new Date();
        event = this.navParams.data;
        this.date = event;
        this.time = this.date;
        let start_hour = event.getHours().toString();
        start_hour = this.addZero(start_hour);
        let start_minutes = event.getMinutes().toString();
        start_minutes = this.addZero(start_minutes);
        let time_start = start_hour + ':' + start_minutes;

        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        let month_start = event.getMonth();
        month_start = month[month_start];

        let date_start = event.getDate().toString();

        this.time = date_start + ' ' + month_start + ' jam ' + time_start;
        

    }

    addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    onSubmit(form){
        let requester = form.value['email'];
        let period = form.value['time'];
        let location = form.value['location'];
        let desc = form.value['desc'];
        let duration = form.value['duration'];

        this.colors = {
            red: {
            primary: '#ad2121',
            secondary: '#FAE3E3'
            },
        }

        let end = new Date(this.date);
        this.number = +end.getHours() + +duration;
        duration += end.getHours();
        
        end = new Date(end.setHours(this.number));

        let meeting = [];
        meeting['email'] = this.to;
        meeting['requester'] = requester;
        meeting['location'] = location;
        meeting['desc'] = desc;
        meeting['status'] = 'requested';
        meeting['color'] = this.colors;
        meeting['date'] = this.date.toString();
        meeting['end'] = end.toString();

    

        this.calendarSvc.addMeeting(meeting).then(data => {
           if(data == true){
                let toast = this.toastCtrl.create({
                    message: 'Request Sent Successfully',
                    duration: 3000,
                    position: 'top'
                });
                this.storage.remove('to');
                toast.present();
                this.navCtrl.setRoot(DashboardPage);
           }   
           else{
            let toast = this.toastCtrl.create({
                message: 'Failed to Sent Request Please Try Again Later',
                duration: 3000,
                position: 'top'
            });
            
            toast.present();
           }
        });
    }
}
