import { userData } from './../../interface/userdata.interface';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,ToastController } from 'ionic-angular';
import { AuthService } from '../../services/authservice';
import { TabsPage } from '../tabs/tabs';

import { ImagePicker } from '@ionic-native/image-picker';
import { Storage } from '@ionic/storage/dist/storage';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFirestore } from 'angularfire2/firestore';
import { Base64 } from '@ionic-native/base64';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  currentuser : userData;
  name : any;
  gender : any;
  dob : any;
  job : any;
  email: any;
  image : any;
  uid : any;
  //userCollection: AngularFirestoreCollection<userData>;
  userDatas: Observable<userData[]>;
  constructor(public navCtrl: NavController, public navParams: NavParams,
                private authService : AuthService,
                private loadingCtrl : LoadingController,
                private imagePicker: ImagePicker,
                public toastCtrl: ToastController,
                public storage: Storage,
                public afd : AngularFireDatabase,
                public afs: AngularFirestore,
                private base64: Base64) {
                 
  }

    ionViewDidLoad() {
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        
        loading.present();
        console.log('ionViewDidLoad ProfilePage');
        
        this.storage.get('uid').then(data => {
            console.log("uid user="+data);
            this.uid=data;
            // this.userCollection=this.afs.collection('/users/'+this.uid+'/userDetails');
            // this.userDatas= this.userCollection.valueChanges();
            // console.log('pake collection');
            // this.userCollection.valueChanges().subscribe(
            //     console.log
            // )
            console.log('pake afd');
            this.authService.getUserDetails(this.uid).valueChanges().subscribe(
                // console.log,
                data=>{
                            console.log(data);
                            this.name = data['name'];
                            this.gender = data['gender'];
                            this.dob = data['dob'];
                            this.job = data['job'];
                            this.email = data['email'];
                            console.log("email="+data['email']);
                            loading.dismiss();
                        },
                        //error=>{console.log(error)}
            )
            // this.afd.object('/users/'+this.uid+'/userDetails').valueChanges().subscribe(
            //     data=>{
            //         console.log(data);
            //         this.name = data['name'];
            //         this.gender = data['gender'];
            //         this.dob = data['dob'];
            //         this.job = data['job'];
            //         loading.dismiss();
            //     },
            //     error=>{console.log(error)}
            // );
        }).catch(error => {
            console.log(error);
        });

        // this.userCollection=this.afs.collection('/users/'+this.uid+'/userDetails');
        // this.userDatas= this.userCollection.valueChanges();
        // console.log("collection="+this.userCollection);
        // this.userCollection.valueChanges(
        //     data=>{

        //     }
        // );
        // let userDetails=this.afd.list('/users/'+this.uid+'/userDetails');
        // userDetails.valueChanges().subscribe(
        //     console.log
        // )

        // this.authService.getActiveUser().getIdToken()
        // .then((token: string) => {
        //     this.authService.getUserData(token)
        //         .subscribe((data) => {
        //             console.log("berhasil get profile");
        //             console.log(data);
        //             this.name = data['name'];
        //             this.gender = data['gender'];
        //             this.dob = data['dob'];
        //             this.job = data['job'];
        //             loading.dismiss();
        //         },
        //         error=>{console.log(error)})
        // })
        // .catch(error => {
        //     console.log(error);
        // })
    }

    onSubmit(form){
        let user = [];
        // user['name'] = form.value['name'];
        // user['gender'] = form.value['gender'];
        // user['dob'] = form.value['dob'];
        // user['job'] = form.value['job'];
        // user['email'] = this.email;
        // this.currentuser = user;
        let updateUser: userData={
            name:form.value['name'],
            gender:form.value['gender'],
            dob:form.value['dob'],
            job:form.value['job'],
            email: this.email,
            uid:this.uid
        }
        this.currentuser=updateUser;
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        
        loading.present();
        this.authService.getActiveUser().getIdToken()
        .then((token: string) => {
            /*this.authService.registrationData(token, this.currentuser)
                .subscribe(() => {
                    console.log("berhasil update profile");
                    loading.dismiss();
                    //this.navCtrl.push(TabsPage);
                    const toast = this.toastCtrl.create({
                        message: "profile updated",
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                }*/
                this.authService.registrationDataFireAngular(updateUser);
                console.log("berhasil update profile");
                loading.dismiss();
                const toast = this.toastCtrl.create({
                    message: "profile updated",
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
                //error=>{console.log(error)})
        })
        .catch(error => {
            console.log(error);
        })
    }

    changeImage()
    {
      //  this.image = 'assets/img/latios.png'
        let filePath: string = this.image;
        let options = {
            maximumImagesCount: 1,
            quality: 100
        }

        this.imagePicker.getPictures(options).then((results) => {
            for (var i = 0; i < results.length; i++) {
                console.log('Image URI: ' + results[i]);
                 let url = results[i];
                 this.image = url;
                //this.image = 'assets\img\latios.png'
                this.base64.encodeFile(url).then((base64File: string) => {
            
                //this.image= base64File;
                this.authService.updatepicture(this.image, this.uid);
                // add base64 file ke firebase
                }, (err) => {
                    console.log('error base 64');
                console.log(err);
                });
            }
          }, (err) => { 
            console.log('error imagepicker');
              console.log(err);
          });
    }

}
