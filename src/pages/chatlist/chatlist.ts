import { Observable } from 'rxjs/Observable';
import { userData } from './../../interface/userdata.interface';
import { AngularFireList } from 'angularfire2/database/interfaces';
import { Chat } from './../chat/chat';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../services/authservice';
import { Storage } from '@ionic/storage/dist/storage';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFirestore } from 'angularfire2/firestore';
import { ChatService } from '../../services/chat-service';
import { ChatData } from '../../interface/chatData.interface';

/**
 * Generated class for the ChatlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatlist',
  templateUrl: 'chatlist.html',
})
export class ChatlistPage {

  toUser:Object;
  userService: any;
  interlucotor: any;
  users:Observable<userData>[];
  chatList:any;
  //chat: any=Chat;
    constructor(public navCtrl: NavController,
        private authService : AuthService,
        public storage: Storage,
        public afd : AngularFireDatabase,
        public afs: AngularFirestore,
        public chatSrvc: ChatService) {

        this.userService=this.authService.getUsers();
        //console.log("interlucotor= "+this.interlucotor);

        this.userService.valueChanges().take(1).subscribe(
          // console.log,
          (users)=>{

            console.log("isi data= "+users);
            console.log("isi subscribe[0]= "+users[0]['name']);

            // let i=0;
            // console.log("while mulai sampe"+ data.length);
            // while(i<data.length){
            //   console.log("isi while= "+data[i]['name']);
            //   this.users.push(data[0]);
            //   continue;
            //   console.log("isi while= "+data[i]['name']);
            //   i++;
            // }
            //let persons=new Array[data.length];
            //console.log("panjang prsn "+ persons.length);

            users.forEach(element => {
              console.log("isi foreach= "+element['name']);
              //this.users.push(element);
            });
            this.users=users;
            this.searchbarEmpty();
            stop();
            return;

            //this.users=data;
          }
        );
    }

    ionViewDidLoad() {
      this.searchbarEmpty();
    }

    searchbarEmpty(){
      console.log('searchbar kosong jalan');
      this.interlucotor=[];
      let uid=this.authService.getActiveUser().uid;
      this.chatSrvc.getChatList(uid).subscribe(
              chatList=>{
                this.interlucotor=[];
                console.log("chatlist="+chatList);
                chatList.forEach(chatData=>{
                  //console.log("foreach kosong="+chatData["interlucotorName"]);
                  let userEmpty=[];
                  userEmpty['name']=chatData["interlucotorName"];
                  console.log('interloc username='+userEmpty['name']);
                  userEmpty['email']=chatData["lastMessage"];
                  console.log('email last message='+userEmpty['email']);
                  userEmpty['gender']='';
                  userEmpty['dob']=null;
                  userEmpty['job']='';
                  userEmpty['uid']=chatData['interlucotorID'];
                  this.interlucotor.push(userEmpty);
                });  
      })


      // this.storage.get('uid').then(
      //   uid=>{
      //     console.log("uid buat search kosong="+uid);
      //     // this.chatSrvc.getChatList(uid).subscribe(
      //     //   chatList=>{
      //     //     //this.chatList=chatList;
      //     //     console.log("chatlist="+chatList);
      //     //     chatList['name']=chatList['interlucotorName'];
      //     //     console.log("interlocutor name="+chatList['interlucotorName']);
      //     //     chatList['email']=chatList['lastMessage'];
      //     //     this.interlucotor=chatList;
      //     //     console.log('interlucotor kosong nama'+this.interlucotor.email);
      //     //   }
      //     // )
      //     this.chatSrvc.getChatList(uid).subscribe(
      //       chatList=>{
      //         console.log("chatlist="+chatList);
      //         // chatList.forEach(chatData=>{
      //         //   console.log("foreach kosong="+chatData["interlucotorName"]);
      //         // });
      //           // if(this.chatData==undefined){
      //           //     this.chatData=data;
      //           //     (console.log('chat module jalan'));
      //           //     this.chatModule();
      //           // }
                    
      //       }
      //   );
      //   })
    }
  
    onInput(searchbar){
      let q=searchbar.srcElement.value;
      console.log("input="+q);
      this.interlucotor=[];
      if(q==''||q==undefined){
        this.searchbarEmpty();
        // console.log('searchbar kosong jalan');
        // this.storage.get('uid').then(
        //   uid=>{
        //     console.log("uid buat search kosong="+uid);
        //     this.chatSrvc.getChatList(uid).subscribe(
        //       chatList=>{
        //         //this.chatList=chatList;
        //         chatList['name']=chatList['interlucotorName'];
        //         console.log("interlocutor name="+chatList['interlucotorName']);
        //         chatList['email']=chatList['lastMessage'];
        //         this.interlucotor=chatList;
        //         console.log('interlucotor kosong nama'+this.interlucotor.email);
        //       }
        //     )
        //   })
        
      }
      else{
        //q=q.toLowerCase();
        console.log('searchbar keisi jalan');
        this.interlucotor=[];
        this.storage.get("uid").then(uid=>{
          this.users.forEach(element=>{
            if(element['name'].toLowerCase().indexOf(q.toLowerCase())>=0){
                //console.log('foreach search= '+element);
                //console.log('foreach search= '+element['name']);
                if(uid!=element['uid']){
                  this.interlucotor.push(element);
                }
              }
              return;
          });
        });
        
      }
    }
    interlocutorClick(interlocutor){
      console.log('orang diklik= '+interlocutor.name);
      if(interlocutor.job==''){
        this.authService.getUserDetails(interlocutor.uid).valueChanges().take(1).subscribe(
          data=>{
            console.log("subscribe dulu");
            console.log("subscribe name="+data['name']);
            let user=[];
            user['name'] = data['name'];
            user['gender'] = data['gender'];
            user['dob'] = data['dob'];
            user['job'] = data['job'];
            user['email'] = data['email'];
            user['uid']=data['uid'];
            //this.interlucotor=user;
            this.navCtrl.push(Chat, user);
          }
        )
      }
      else this.navCtrl.push(Chat, interlocutor);
    }
}
  