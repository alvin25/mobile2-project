import { Storage } from '@ionic/storage';
import { AuthService } from './../../services/authservice';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Keyboard } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    
    constructor(public navCtrl: NavController, 
        public navParams: NavParams,
        public authSrvc:AuthService,
        public toastCtrl: ToastController,
        private loadingCtrl : LoadingController,
        public storage : Storage) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    onSubmit(form){
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        
        loading.present();

        console.log(form.value);
        this.authSrvc.login(form.value['email'],form.value['password'])
        .then(data =>{
            console.log(data);    
            let uid=this.authSrvc.getActiveUser().uid;
            this.storage.set('uid',uid);
            this.storage.set('email', data.email);
            console.log("user uid="+uid);
            loading.dismiss();
            this.navCtrl.setRoot(TabsPage);
        })
        .catch(err=>{
            loading.dismiss();
            const toast = this.toastCtrl.create({
                message: err.message,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        })
    }

    registerAccount(){
        this.navCtrl.push(RegisterPage);
    }
}
