export class ChatMessage {
    //messageId: string;
    fromUserEmail: string;
    fromUserName: string;
    //userAvatar: string;
    toUserEmail: string;
    toUserName: string;
    time: number | string;
    message: string;
    status: boolean;
}