import { Chat } from './../pages/chat/chat';
import { ChatlistPage } from './../pages/chatlist/chatlist';
import { ChatService } from './../services/chat-service';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule  } from '@ionic/storage';

import { ImagePicker } from '@ionic-native/image-picker';

import { MyApp } from './app.component';
import { AuthService } from '../services/authservice';
import { CalendarService } from '../services/calendarservice';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ProfilePage } from '../pages/profile/profile';
import { TabsPage } from '../pages/tabs/tabs';
import { CalendarPage } from '../pages/calendar/calendar';
import { DayPage } from '../pages/day/day';
import { RequestformPage } from '../pages/requestform/requestform';
import { ModalPage } from '../pages/modal/modal';

import { HttpModule } from '@angular/http';
import { CalendarModule } from 'angular-calendar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import  firebase  from 'firebase';
import { EmojiProvider } from '../services/emoji';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { Base64} from '@ionic-native/base64';
import { Calendar } from '@ionic-native/calendar';

import { Firebase } from '@ionic-native/firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAbLoj7wLvSfwGf7PTUKnGWkjoJ3dLx748",
    authDomain: "temu-a6ded.firebaseapp.com",
    databaseURL: "https://temu-a6ded.firebaseio.com",
    projectId: "temu-a6ded",
    storageBucket: "temu-a6ded.appspot.com",
    messagingSenderId: "83714152575"
  };

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        LoginPage,
        RegisterPage,
        DashboardPage,
        ProfilePage,
        RequestformPage,
        ChatlistPage,
        Chat,
        TabsPage,
        RegisterPage,
        CalendarPage,
        DayPage,
        ModalPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        BrowserAnimationsModule,
        CalendarModule.forRoot(),
        AngularFireDatabaseModule,
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFirestoreModule,
        IonicStorageModule.forRoot(),
        IonicModule.forRoot(MyApp)
        
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        LoginPage,
        RegisterPage,
        DashboardPage,
        ProfilePage,
        ChatlistPage,
        RequestformPage,
        Chat,
        TabsPage,
        RegisterPage,
        CalendarPage,
        DayPage,
        ModalPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        AuthService,
        ImagePicker,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        ChatService,
        EmojiProvider,
        FirebaseProvider,
        CalendarService,
        IonicStorageModule,
        Base64,
        Calendar,
        Firebase
    ]
})
export class AppModule {}
