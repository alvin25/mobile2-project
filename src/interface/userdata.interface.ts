export interface userData{
    name: string;
    gender: string;
    dob: Date;
    job: string;
    email: string;
    uid: string;
}