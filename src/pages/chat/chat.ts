import { AuthService } from './../../services/authservice';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, LoadingController } from 'ionic-angular';
import { Events, Content, TextInput } from 'ionic-angular';
import { ChatService, UserInfo } from "../../services/chat-service";
import { Storage } from '@ionic/storage';
import { userData } from '../../interface/userdata.interface';
import { ChatMessage } from '../../interface/chatMessage.interface';

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class Chat {

    @ViewChild(Content) content: Content;
    @ViewChild('chat_input') messageInput: TextInput;
    msgList: any;
    currentuser : userData[];
    interlocutor: any;
    editorMsg = '';
    showEmojiPicker = false;
    firstTimeMessage=false;
    chatID: string;
    chatData: any;

    constructor(public navParams: NavParams,
                public chatService: ChatService,
                public loadingCtrl: LoadingController,
                public events: Events,
                private authService : AuthService,
                public storage: Storage) {
        // Get the navParams toUserId parameter
        this.interlocutor=this.navParams.data;
        
        console.log("lawan bicara="+this.navParams.data);
        console.log("lawan bicara="+this.interlocutor);
        //console.log("lawan bicara="+this.interlocutor.name+' '+this.interlocutor.uid);

        // Get mock user information
        // this.chatService.getUserInfo()
        // .then((res) => {
        //     this.user = res
        // });
    }

    ionViewWillLeave() {
        // unsubscribe
        //this.events.unsubscribe('chat:received');
    }

    ionViewDidEnter() {
        //this.getUserInfo();
        //get message list
        // this.getMsg()
        // .then(() => {
        //     this.scrollToBottom();
        // });

        // // Subscribe to received  new message events
        // this.events.subscribe('chat:received', msg => {
        //     this.pushNewMsg(msg);
        // })
    }
    ionViewDidLoad() {
        if(this.interlocutor.name==undefined||this.interlocutor.uid==''||this.interlocutor.gender==''){
            this.authService.getUserDetails(this.interlocutor.interlucotorID).valueChanges().take(1).subscribe(
                userData=>{
                    this.interlocutor=userData;
                    this.getUserInfo()
                }
            )
        }
        else
            this.getUserInfo()
        
        // if(this.isChatExist()){
        //     console.log("exist true")
        // }else{
        //     console.log("exist false");
        // }
    
        
        //this.getMsg();
    }

    onFocus() {
        // this.showEmojiPicker = false;
        this.content.resize();
        this.content.scrollToBottom();
    }

    // switchEmojiPicker() {
    //     this.showEmojiPicker = !this.showEmojiPicker;
    //     if (!this.showEmojiPicker) {
    //         this.messageInput.setFocus();
    //     }
    //     this.content.resize();
    //     this.scrollToBottom();
    // }

    getUserInfo(){
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.storage.get('uid').then(data => {
            console.log("uid user="+data);
            let uid=data;
            // let name;
            // let gender;
            // let dob;
            // let job;
            // let email;
            console.log('pake afd');
            this.authService.getUserDetails(uid).valueChanges().take(1).subscribe(
                data=>{
                            console.log("data="+data);
                            console.log("data.nama="+data['name']);
                            let user=[];
                            user['name'] = data['name'];
                            user['gender'] = data['gender'];
                            user['dob'] = data['dob'];
                            user['job'] = data['job'];
                            user['email'] = data['email'];
                            user['uid']=data['uid'];
                            this.currentuser=user;
                            loading.dismiss();

                            this.isChatExist();

                            //return true;
                        }
            )
            //return;
        }).catch(error => {
            console.log(error);
        });
        //return true;
    }

    isChatExist(){
        let uid=this.currentuser['uid'];
        let iUid=this.interlocutor['uid'];
        console.log('1');
        //let exist:string="0";
        this.chatService.getChatList(this.currentuser['uid']).take(1).subscribe(
            data=>{
                if(this.chatData==undefined){
                    this.chatData=data;
                    (console.log('chat module jalan'));
                    this.chatModule();
                }
                    
                return;
            }
        );
        // if(exist!="0"){
        //     console.log("masuk if true");
        //     return true;
        // }else{
        //     console.log("masuk if false");
        //     return false;
        // }

    }

    chatModule(){
        let uid=this.currentuser['uid'];
        let iUid=this.interlocutor['uid'];
        let flag=true;
        this.chatData.forEach(element=>{
            console.log("element chatlist="+element.interlucotorID);
            console.log("interloc chatlist="+iUid);
            if(element['interlucotorID'].indexOf(iUid)>=0){
                //kalo udah pernah chat 1
                console.log("udah pernah chat");
                console.log("chatID="+element.chatID);
                this.chatID=element.chatID;
                this.getMsgAlt();
                this.scrollToBottom();
                console.log("getMsgAlt jalan");
                flag=false;
                // this.chatService.getChatId(uid).subscribe(
                //     data=>{
                //         data.forEach(element=>{
                //             console.log('element chatid='+element.uid);
                //             if(this.interlocutor['uid'].indexOf(element['uid'])>=0){
                //                 this.chatID=element['chatID'];
                //                 this.getMsgAlt();
                //                 return;
                //             }
                //             return;
                //         });
                //     }
                // );
                //this.getMsgAlt();
            }
            console.log("loop");
        });
        if(flag){
            console.log("belom pernah chat");
            this.chatID=this.chatService.firstTimeChat(uid,iUid,this.currentuser,this.interlocutor);
            this.getMsgAlt();
            this.scrollToBottom();
        }
        return;
    }

    sendMsgAlt(){
        console.log("chat send clicked");
        if (!this.editorMsg.trim()) return;
            let currentUserMsg: ChatMessage = {
                fromUserEmail: this.currentuser['email'],
                fromUserName: this.currentuser['name'],
                toUserEmail: this.interlocutor['email'],
                toUserName: this.interlocutor['name'],
                time: Date.now(),
                message: this.editorMsg,
                status: false
            };
            this.chatService.sendMsgAlt(this.chatID, currentUserMsg, this.currentuser, this.interlocutor);
            this.editorMsg=" ";
    }

    sendMsg(){
        console.log("chat send clicked");
        if (!this.editorMsg.trim()) return;
        
            // Mock message
            // const id = Date.now().toString();
            let currentUserMsg: ChatMessage = {
                //messageId: Date.now().toString(),
                fromUserEmail: this.currentuser['email'],
                fromUserName: this.currentuser['name'],
                toUserEmail: this.interlocutor['email'],
                toUserName: this.interlocutor['name'],
                time: Date.now(),
                message: this.editorMsg,
                status: false
            };
            // let interlocutorMsg: ChatMessage = {
            //     //messageId: Date.now().toString(),
            //     userEmail: this.currentuser['email'],
            //     userName: this.currentuser['name'],
            //     toUserEmail: this.interlocutor['email'],
            //     toUserName: this.interlocutor['name'],
            //     time: Date.now(),
            //     message: this.editorMsg,
            //     status: false
            // };

           
            this.chatService.sendMsg(this.interlocutor['uid'], currentUserMsg);
            this.editorMsg=" ";
    }

    getMsgAlt(){
        this.chatService.getMsgAlt(this.chatID).subscribe(
            data=>{
                data.forEach(element=>{
                    console.log("isi chat="+element['message']);
                    if(element['message']==''||element['message']==undefined){
                        let ctr=data.indexOf(element);
                        data.splice(ctr,2);
                    }
                });
                this.msgList=data;
            }
        );
    }

    /**
     * @name getMsg
     * @returns {Promise<ChatMessage[]>}
     */
    getMsg() {
        this.chatService.getMsg(this.interlocutor['uid']).subscribe(
            data=>{
                //this.msgList.push
                data.forEach(element=>{
                    console.log("isi chat="+element['message']);
                })
                this.msgList=data;
            }
        )

        // Get mock message list
        // return this.chatService
        // .getMsgList()
        // .then(res => {
        //     this.msgList = res;
        // })
        // .catch(err => {
        //     console.log(err)
        // })


    }

    /**
     * @name sendMsg
     */
    // sendMsg() {
    //     if (!this.editorMsg.trim()) return;

    //     // Mock message
    //     const id = Date.now().toString();
    //     let newMsg: ChatMessage = {
    //         messageId: Date.now().toString(),
    //         userId: this.user.id,
    //         userName: this.user.name,
    //         userAvatar: this.user.avatar,
    //         toUserId: this.toUser.id,
    //         time: Date.now(),
    //         message: this.editorMsg,
    //         status: 'pending'
    //     };

    //     this.pushNewMsg(newMsg);
    //     this.editorMsg = '';

    //     if (!this.showEmojiPicker) {
    //         this.messageInput.setFocus();
    //     }

    //     this.chatService.sendMsg(newMsg)
    //     .then(() => {
    //         let index = this.getMsgIndexById(id);
    //         if (index !== -1) {
    //             this.msgList[index].status = 'success';
    //         }
    //     })
    // }

    // /**
    //  * @name pushNewMsg
    //  * @param msg
    //  */
    // pushNewMsg(msg: ChatMessage) {
    //     const userId = this.user.id,
    //           toUserId = this.toUser.id;
    //     // Verify user relationships
    //     if (msg.userId === userId && msg.toUserId === toUserId) {
    //         this.msgList.push(msg);
    //     } else if (msg.toUserId === userId && msg.userId === toUserId) {
    //         this.msgList.push(msg);
    //     }
    //     this.scrollToBottom();
    // }

    // getMsgIndexById(id: string) {
    //     return this.msgList.findIndex(e => e.messageId === id)
    // }

    scrollToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 400)
    }
}
