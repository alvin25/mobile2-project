import { AngularFireList } from 'angularfire2/database/interfaces';
import { userData } from './../interface/userdata.interface';
import firebase from 'firebase';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { AngularFireDatabase  } from 'angularfire2/database';
import { Storage  } from '@ionic/storage';
import { query } from '@angular/core/src/animation/dsl';
import { Promise } from 'q';

@Injectable()
export class CalendarService{

    constructor(private http:Http,
        private afd: AngularFireDatabase,
        private storage:Storage ){

    }

    addMeeting(event) : Promise<any>{
        console.log(event);
        return Promise(resolve =>
            {
                let key = this.afd.list(`/meetingDetails`).push(event).key;
                console.log(key);
                event['key'] = key;
                console.log(event);
                this.afd.object('/meetingDetails/'+ key).set(event).then(data =>{
                    resolve(true);
                })
            });
    }

    getAllMeeting(){
        return this.afd.list('/meetingDetails').valueChanges();
    }

    updateMeeting(data){
        let key = data['key'];
        let meeting = this.afd.object('/meetingDetails/'+key).update(data);
        //console.log(meeting.valueChanges());
        //meeting.set('/meetingDetails',data);
    }
}