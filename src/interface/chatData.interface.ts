export class ChatData {
    chatID:string;
    interlucotorID: string;
    interlucotorName:string;
    interlucotorEmail: string;
    lastMessage:string;
    date:string | number;
}