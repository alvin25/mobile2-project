import { AngularFireList } from 'angularfire2/database/interfaces';
import { userData } from './../interface/userdata.interface';
import firebase from 'firebase';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs';
import 'rxjs/add/operator/take'
import { Injectable, Inject } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { AngularFireDatabase  } from 'angularfire2/database';
import { Storage  } from '@ionic/storage';
import { query } from '@angular/core/src/animation/dsl';

@Injectable()
export class AuthService{

    constructor(private http:Http,
                private afd: AngularFireDatabase,
                private storage:Storage ){

    }

    register(email: string, password: string) 
    {
        return firebase.auth().createUserWithEmailAndPassword(email,password);
    }
    login(email:string, password: string) 
    {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }
    logout() {
        firebase.auth().signOut();
    }
    getActiveUser(){
        return firebase.auth().currentUser;
    }

    registrationDataFireAngular(userData){
        let uid = this.getActiveUser().uid;
        this.storage.set('uid', uid);
        userData['uid']=uid;
        console.log("uid user="+uid);
        //this.afd.object(`/users/${uid}/userDetails`).set(userData);
        this.afd.object(`/userDetails/${uid}`).update(userData);
    }

    getUserDetails(uid){
        return this.afd.object('/userDetails/'+uid);

        // let userDetails;
        // return userDetails=
        // this.storage.get('uid').then(data => {
        //     console.log("uid user="+data);
        //     userDetails=this.afd.object('/users/'+data+'/userDetails');
        //     return userDetails;
        // });
        // let userDetails=this.afd.list('/users/'+uid+'/userDetails');
        // return userDetails;
    }

    getUsers(){
        //return this.afd.list('/userDetails', query=>query.orderByChild('email').equalTo('asd'));
        return this.afd.list('/userDetails');
    }

    // getChatList(uid){
    //     return this.afd.object('/chatList')
    // }

    // registrationData(token: string, currentuser:userData[]){
    //     const uid=this.getActiveUser().uid;
    //     return this.http
    //         .put('https://temu-a6ded.firebaseio.com/'+uid+
    //         '/UserData.json?auth='+
    //         token, currentuser)
    //         .map((response:Response)=>{
    //             return response.json();
    //         })
    // }
    registrationData(token: string, currentuser:userData[]) : Observable<any>
    {
        const uid=this.getActiveUser().uid;
        console.log(uid);
        console.log(token);
        console.log(currentuser);
        let url = 'https://temu-a6ded.firebaseio.com/'+uid+
            '/user-data.json?auth='+token;
        console.log(url);
        let header = new Headers();
        let body = {
            name : currentuser['name'],
            gender : currentuser['gender'],
            dob : currentuser['dob'],
            job : currentuser['job']
        };
        console.log(body);
        header.append('Content-Type', 'application/json');
        return this.http
            .put(url, JSON.stringify(body), {headers:header})
            .map((response:Response)=>{
                console.log(response.json());
                return response.json();
            })
    }

    

    getUserData(token : string) : Observable<any>{
        const uid=this.getActiveUser().uid;
        let url = 'https://temu-a6ded.firebaseio.com/users/'+uid+
            '/userDetails.json?auth='+token;
        console.log(url);
        return this.http
            .get(url)
            .map((response:Response)=>{
                console.log(response.json());
                return response.json();
            })
    }

    updatepicture(image, uid){
        let img =[];
        img['image']=image;
        this.afd.object('/userDetails/'+ uid).update('asdasdasdad');
    }
    
}
