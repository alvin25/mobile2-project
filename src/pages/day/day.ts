import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { CalendarEvent } from 'angular-calendar';
import {RequestformPage} from '../requestform/requestform';

/**
 * Generated class for the DayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-day',
  templateUrl: 'day.html',
})
export class DayPage {
    date : Date;
    allEvent : CalendarEvent[];

    colors : any;
    event : CalendarEvent;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                private alertCtrl : AlertController) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DayPage');
    }

    ngOnInit(){
        let event = this.navParams.data;
        this.date = event['date'];
        console.log(this.date);
        
        this.allEvent = event['events'];
    }

    request(event){
        console.log(event);
        this.navCtrl.push(RequestformPage,event.date);
    }

}
